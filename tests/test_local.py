import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ics-ans-role-eee-local')


def test_rsync_epics_service_enabled(Service):
    service = Service("rsync-epics.service")
    # Don't check that the service is running because
    # it doesn't run forever
    assert service.is_enabled


def test_rsync_epics_timer_running_and_enabled(Service):
    timer = Service("rsync-epics.timer")
    assert timer.is_running
    assert timer.is_enabled


def test_eee_local_directories(File):
    assert File('/opt/epics/modules').is_directory
    assert not File('/opt/startup/boot').exists
    assert not File('/opt/eldk-5.6/ifc1210').exists
