import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ics-ans-role-eee-default')


def test_eee_access(File):
    assert File('/opt/epics/modules').is_directory
    assert File('/opt/startup/boot').is_directory
    assert File('/opt/eldk-5.6/ifc1210').is_directory
