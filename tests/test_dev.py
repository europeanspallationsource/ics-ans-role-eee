import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ics-ans-role-eee-default')


def test_epics_environment(Command):
    cmd = Command('source /etc/profile && env')
    env = cmd.stdout
    assert 'EPICS_ENV_PATH=/opt/epics/modules/environment/2.0.0/3.15.4/bin/centos7-x86_64' in env
    assert 'EPICS_BASE=/opt/epics/bases/base-3.15.4' in env
    assert 'EPICS_DB_INCLUDE_PATH=/opt/epics/bases/base-3.15.4/dbd' in env


def test_vdct(Command):
    cmd = Command('cd /opt/VisualDCT && ./runScript --help')
    assert cmd.stdout.startswith('VisualDCT help')
