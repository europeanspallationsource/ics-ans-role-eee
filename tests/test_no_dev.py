import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ics-ans-role-eee-no-dev')


def test_no_epics_env(File):
    assert not File('/etc/profile.d/ess_epics_env.sh').exists


def test_no_vdct(File):
    assert not File('/opt/VisualDCT').exists


def test_no_dev_packages(Package):
    assert not Package('libusb-devel').is_installed
    assert not Package('hdf5-devel').is_installed
