import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ics-ans-role-eee-dev-physical')


def test_procserv(Command):
    cmd = Command('/usr/bin/procServ --version')
    assert cmd.stdout.startswith('procServ Process Server')


def test_autostart_ioc_service_enabled(Service):
    for name in ('ess-boot.service', 'ioc-master.service', 'procServ-vacuum.timer'):
        assert Service(name).is_enabled
